<?php
$this->load->helper('form');
function radio_selector($name,$choices) {
  foreach ($choices as $choice) {
    echo form_radio($name, $choice);
  }
}
?>
<div id="container">
 <h1>新規登録</h1>
 <div id="body">
 <?=form_open('term/new')?>
  <table>
   <tr><td>用語        </td><td><?=form_input('J_term')?></td></tr>
   <tr><td>重要度      </td><td><?=form_dropdown('priority', $opt["priority"])?></td></tr>
   <tr><td>分野        </td><td><?=form_dropdown('dicipline')?></td></tr>
   <tr><td>解説詳度    </td><td><?=form_radio('article_type')?></td></tr>
  </table>
  <?=form_submit('submit', '登録')?>
 </form>
 </div>
</div>
