<!-- vlist_terms.php -->
<?php $get="term/get"?>
<?php $get="term/find"?>
<?php $listing="term/listing"?>
Hit: <?=$query->num_rows()?>
<table>
<!-- table header -->
<tr>
  <th>ID</th>
  <th>英用語</th>
  <th width=26 title='属性に記入のある用語一覧が表示されます'>
   <?=anchor("$listing/addition",'属性')?></th>
  <th width=120 title='訳語に記入のある用語一覧が表示されます'>
   <?=anchor("$listing/translation_j",'訳語')?></th>
  <th title='訳語に記入のある用語一覧が表示されます'>
   <?=anchor("$listing/explanation",'説明')?></th>
  <th width=50 title='重要度に記入のある用語一覧が表示されます'>
   <?=anchor("$listing/category","重要度")?></th>
  <th width=32 title='分野に記入のある用語一覧が表示されます'>
   <?=anchor("$listing/category_2","分野")?></th>
  <th title='関連語に記入のある用語一覧が表示されます'>
   <?=anchor("$listing/relatedword","関連語")?></th>
  <th width=68 title='用語集に記入のある用語一覧が表示されます'>
   <?=anchor("$listing/for_glossary","用語集")?></th>
  <th width=80 title='用語項目に記入のある用語一覧が表示されます'>
   <?=anchor("$listing/for_glossary2","用語項目")?></th>
  <th width=20 title='スコア順に用語一覧が表示されます'>
   <?=anchor("$listing/avr_score","ｽｺｱ")?></th>
   <?php if ($query->num_rows()>0 and isset($query->row()->comment)) {
    echo "<th>";
    echo anchor("$listing/comment","Comment");
    echo "</th>";
  }?>
</tr>

<!-- table data -->
<?php foreach ($query->result() as $row):?>
<tr>
   <td title='用語詳細を表示します'>
   <?=anchor("$get/id/".$row->id,$row->id)?></td>
   <td title='関連用語を検索します'>
   <?php $term_e=preg_replace('|\+|','%2b',$row->term_e);?>
   <?php if ($row->term_e==='') $row->term_e = ' ';?>
   <?=anchor("$get/term_e/".$term_e,$row->term_e)?></td>
   <td title='関連用語を検索します'><?=($row->addition !== '')
   ? anchor("$get/addition/".$row->addition,$row->addition).' '
    : ''?></td>
  <?php $term_j=rawurlencode($row->translation_j);?>
  <td title='関連用語を検索します'><?=anchor("$get/translation_j/$term_j",$row->translation_j.' ')?></td>
  <td title='関連用語を検索します'><?=$row->explanation.' '?></td>
  <td title='関連用語を検索します'><?=($row->category!=='')?
  anchor("$get/category/$row->category",$row->category.' '):''?></td>
  <td title='関連用語を検索します'><?=($row->category_2!=='')?
  anchor("$get/category_2/$row->category_2",$row->category_2.' '):''?></td>
  <td title='関連用語を検索します'><?=($row->relatedword !== '')?
   anchor("$get/term_e/".rawurlencode($row->relatedword),
	   $row->relatedword.' '):''?></td>
  <td title='関連用語を検索します'><?=($row->for_glossary !== '')
    ? anchor("$get/for_glossary/$row->for_glossary",$row->for_glossary.' ')
    : ''?></td>
  <td title='関連用語を検索します'><?=($row->for_glossary2 !== '')
    ? anchor("$get/for_glossary2/$row->for_glossary2",$row->for_glossary2.' ')
    : ''?></td>
  <td title='関連用語を検索します'><?=sprintf("%.2f",$row->avr_score)?></td>
  <?php if (isset($row->comment)) {
    echo "<th>";
    echo anchor("$get/id/".$row->id, $row->comment.' ');
    echo "</th>";
  }?>
</tr>
<?php endforeach?>
</table>