<div style="clear: both">
 | <span title='ホームに戻ります'>
   <?=anchor(site_url('main/home'),"ﾎｰﾑ")?></span>
 | <span title='詳しく説明する用語の一覧を表示します'>
   <?=anchor(site_url('main/glossary2_summary'),'詳解語')?></span>
 | <span title='最近編集された語を表示します'>
   <?=anchor(site_url('main/recent'),  "最近")?></span>
 | <span title='全ての用語を一覧表示します（時間がかかります）'><?=anchor(site_url("main/list_terms/"),"全覧") ?></span>
 | <span title='用語をダウンロードします'><?=anchor(site_url("download"),"DL") ?></span>
 | <span title='旧サイトを開きます'>
   <?=anchor('http://genetics.ibio.jp/terms/bin/view','旧ｻｲﾄ','target=prev')?></span>
   <?php if ($this->tank_auth->is_logged_in()): ?>
     | <span title='こちらから新語登録できます'>
       <?=anchor(site_url('main/new_term'),'新語')?></span>
     | <span title='更新・削除された用語の履歴'>
       <?=anchor(site_url('main/deleted_term'),'削歴')?></span>
   <?php endif ?>
 | <?php $this->load->view('vuser_state');?>
<br/>
<!-- インデックス A-Z -->
<?php foreach (range('A', 'Z') as $head):?>
 |<span title='<?=$head?>で始まる用語一覧が表示されます'>
   <?=anchor(site_url("main/list_terms/$head"), " $head ")?></span>
<?php endforeach?>|
</div>
