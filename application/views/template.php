<!DOCTYPE html>
<?php
header('Content-Type: text/html; charset=utf-8');
$assoc_title = "<a target='gsj' href='http://gsj3.jp/'>"
		."日本遺伝学会 </a>";
$site_title = '遺伝学用語集編纂プロジェクト';
$default_title =$assoc_title . $site_title;

if ( ! isset($title))    $title = $default_title;
if ( ! isset($subtitle)) $subtitle = '';
if ( ! isset($contents)) $contents = array();
?>

<html lang="ja">
<head>
  <meta charset="utf-8" encoding='utf-8'>
  <title><?=$site_title?></title>

  <link rel="shortcut icon" href="<?=base_url()?>favicon.ico">
  <link rel="stylesheet" href="<?=base_url()?>css/base.css" type="text/css">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
  <script>
    $(document).ready(function() {
      $("#news").load("<?=base_url()?>NEWS.txt");
    });
  </script>
  <?=isset($script) ? '<script>'.$script.'</script>' : ''?>
</head>

<body>
<div id="container">
   <div style="clear:both"></div>
   <span class="buttons">
    <?php $this->load->view('vbuttons');?>
   </span>
  <h1>
   <span id="searchbox" title='全項目に対してキーワード検索できます'>
    <?=form_open('term/search')?>
    <?=form_input('keyword',null,"size=10")?>
    <?=form_submit('search','検索')?>
    </form>
   </span>
   <span class="title"><?=$title ?></span>
  </h1> 

  <div id="news"></div>
  <!-- body -->
  <div id="body">
  <h3><b style="color: red;"><?=$subtitle?></b></h2>
  <?php foreach ($contents as $c): ?>
  <?php echo $c ?>
  <?php endforeach ?>
  </div>
  <div class="footer-right">
  &copy; 日本遺伝学会 2012-2018 / サイト管理者 gsj@ibio.jp |
  <!--   <?=anchor('http://genetics.ibio.jp','本家','target=w1')?> -->
    Page rendered in <strong>{elapsed_time}</strong> seconds.
  </div>
</div>

</body>
</html>
