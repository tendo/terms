<?php $this->load->helper('form')?>
<div id="container">
 <h1>用語登録</h1>
 <div id="body">
  <?=validation_errors()?>
  <?=form_open('term/new')?>
  <table>
   <tr><td>用語（英）  </td><td><?=form_input('E_term')?></td></tr>
   <tr><td>属性        </td><td><?=form_input('E_note')?></td></tr>
   <tr><td>訳語        </td><td><?=form_input('J_term')?></td></tr>
   <tr><td>発音        </td><td><?=form_input('E_pronunciation')?></td></tr>
   <tr><td>説明        </td><td><?=form_input('E_note')?></td></tr>
   <tr><td>重要度      </td><td><?=form_radio('priority')?></td></tr>
   <tr><td>分野        </td><td><?=form_dropdown('dicipline')?></td></tr>
   <tr><td>同義語（英）</td><td><?=form_dropdown('E_synonym')?></td></tr>
   <tr><td>同義語      </td><td><?=form_input('J_synonym')?></td></tr>
   <tr><td>反意語      </td><td><?=form_input('E_antonym')?></td></tr>
   <tr><td>語群        </td><td><?=form_dropdown('group')?></td></tr>
   <tr><td>関連語      </td><td><?=form_dropdown('related')?></td></tr>
   <tr><td>解説詳度    </td><td><?=form_radio('article_type')?></td></tr>
   <tr><td>解説記事    </td><td><?=form_input('article')?></td></tr> 
   <tr><td>重要度      </td><td><?=form_radio('score')?></td></tr>
  </table>
  <?=form_submit('submit', '登録')?>
 </form>
 </div>
</div>
