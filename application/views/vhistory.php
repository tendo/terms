<div>
<?php if ($history->num_rows()>0):?>
<table>
<caption>
更新履歴
</caption>
<tr>
<th>復元</th>
<th>Term #</th>
<th>英用語</th>
<th>訳語</th>
<th>説明</th>
<th>分類</th>
<th>同義語(英)</th>
<th>同義語</th>
<th>更新</th>
<th>削除</th>
</tr>
<?php foreach ($history->result() as $rows): ?>
<tr>
<td><?=anchor(site_url('/term/revert/'.$rows->archive_id),
	      $rows->archive_id)?></td>
<td><?=$rows->id?></td>
<td><?=$rows->term_e?></td>
<td><?=$rows->translation_j?></td>
<td><?=$rows->explanation?></td>
<td><?=$rows->category_2?></td>
<td><?=$rows->synonim_e?></td>
<td><?=$rows->synonim_j?></td>
<td><?=substr($rows->update,0,16)?></td>
<td><?=substr($rows->deleted,0,16)?></td>
</tr>
<?php endforeach?>
</table>
<?php endif; ?>
</div>