<?php
     if ( ! isset($width)) $width = 50;
     $term_id = $query->row()->id;
?>

<?=form_open('term/update')?>
<table>
<?php $get=site_url('term/find');?>
<?php foreach ($query->result_array() as $row):?>
  <?php foreach ($row as $key => $val):?>
   <?php if ($key=="text") {echo form_hidden($key, $val); continue;} ?>
  <tr>
     <td><?=($val!=='')?anchor("$get/$key/$val",$tag[$key]):$tag[$key]?></td>
     <td><?=$key?></td>
     <td><?=(strlen($val)>=$width or $key=="explanation")
              ?form_textarea($key, $val, "cols=$width")
              :form_input   ($key, $val, "size=$width")
         ?>
     </td>
  </tr>
  <?php endforeach?>
<?php endforeach?>
</table>

<?=form_submit('submit','更新')?>
<?=form_submit('delete','削除')?>
<div style='color: brown'>
更新ボタンをクリックすると即座に登録され、再び編集可能になります。<br/>
書き込まれると古いデータは「更新履歴」↓に現れます。<br/>
</div>

<?=isset($DEBUG)?"（未実装のため追加できません）":"" ?>
</form>
<?php $this->load->view('vhistory.php', $history)?>

<div id="reference">
       <a href="http://genetics.ibio.jp/terms/bin/view/Main/GeneticsTerm<?=$term_id?>" target='term_info'>原データ照会<img src="http://genetics.ibio.jp/terms/pub/Main/WebHome/genetics-140x40.gif" alt="[原データ照会]"></a>
</div>



<!-- comments -->
<div id=comment>
<h3 style="color:darkblue">コメント</h3>
<?php foreach ($comments->result() as $row):?>
<code><pre>
 <div>
  <?php
       if ( $row->user_id == $user_id ) {
	 echo form_open('comment/delete');
       }
  ?>
  #<?=$row->comment_id?>
  <?=$row->username?>
  on <?=substr($row->update,0,19)?>
  term# <?=$row->term_id?>
 <?php
       if ( $user_id != 0 and $row->user_id == $user_id ) {
	 echo form_hidden('user_id',$user_id);
	 echo form_hidden('term_id',$term_id);
	 echo form_hidden('comment_id',$row->comment_id);
	 echo form_submit('delete','削除');
	 echo form_close();
       }
 ?>
 </div>
 <div><?=$row->comment?></div>
</pre></code>
<?php endforeach?>     
</div>


<?php if ($deleted_comments->num_rows()) {
 $p["result"] = $deleted_comments->result();
  $this->load->view('vlist_deleted_comments', $p);
}
?>
<!-- add comment form -->
<div>
<?=form_open("comment/add")?>
<?=form_hidden('term_id',$term_id)?>
<?=form_textarea(array("name"=>"comment","cols"=>$width+18))?>
<br/>
<?=form_submit('submit','コメント追加')?>
<?=isset($DEBUG)?"（未実装のため追加できません）":"" ?>
</form>

<!-- placeholder for external reference -->

<script type="text/javascript">
$(function(){
  var ref="http://genetics.ibio.jp/terms/bin/view/Main/GeneticsTerm";
  $("#reference").load(ref+"<?=$term_id?>");
});
</script>
</div>
<!--end of comment -->