<span>
<?php
if ($this->tank_auth->is_logged_in()) {
  // logged in
  $username=$this->tank_auth->get_username();
  $user_id = $this->tank_auth->get_user_id();
  $user = $username ? $username : 'user_'.$user_id;
  echo '<span title="自分が登録した語の一覧が表示されます">';
  echo anchor(site_url('term/get/user/'.$user_id), $user).'さん</span>';
  echo '<span title="ログアウトできます">';
  echo anchor(site_url('logout'),'<span class="em">ﾛｸﾞｲﾝ中</span>');
  echo '</span>';
} else {
  // not logged in
  echo anchor(site_url('auth/login'),'ﾛｸﾞｲﾝ (登録)');
}
?>
</span>
