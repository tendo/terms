<?php
class Mdb extends CI_Model {
  var $term_table    = 'rawdata';
  var $archive_table    = 'rawdata_archive';

  function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function get($field, $target) {
    if ($field == 'term_id') $field='id';
    $target = rawurldecode($target);
    return $this->db->get_where($this->term_table,
				array($field => $target));
  }

  public function find($field, $target){
    if ($field=='id' or $field=='user_id' or $field=='update') {
      return $this->get($field,$target);
    }
    $target = rawurldecode($target);
    $target = mb_convert_kana($target, 's');
    $words = preg_split("/ /", $target);
    $condition = "$field ILIKE ". implode(" OR $field ILIKE ", $words);
    //    foreach ($words as $term) {
    //      $this->db->or_like($field, $term);
    //    }
    return $this->db->get($this->term_table);
  }

  public function listing($field) {
    if ($field == 'avr_score') {
      $this->db->order_by("$field DESC, term_e ASC");
    } else { 
      $this->db->order_by("$field, term_e");
    }
    return $this->db->get_where($this->term_table,
				array("$field !=" => ''));
  }

  function current_id() {
    $query = $this->db->query('last_value from '.$term_table.'_id_seq');
    return 1 + $query->row();
  }

  function is_valid($data) {
    $to_check = array('id', 'term_e');
    foreach ($to_check as $field) {
      if ( ! isset($data[$field])) return FALSE;
      $this->db->where($field,$data[$field]);
    }
    $num = $this->db->count_all_results($this->term_table);
    return ($num == 1);
  }

  public function insert($data, $term_id=NULL) {
    if ( isset($data["id"])) unset($data["id"]);
    if ($term_id != NULL) {
      $data["id"] = $term_id;

      // need checking of existing entry
      $tid = $this->get('term_id', $term_id);
      if ($tid->num_rows()>0) {
	$this->update($data);
	return;
      }       
    }
    
    $data["user_id"] = $this->tank_auth->get_user_id();
    $this->db->insert($this->term_table, $data);
    //    $insert_id = $this->db->insert_id();
    return $this->db->affected_rows()>0 ? TRUE : FALSE;
  }

  public function delete($id, $user_id) {
    if ( ! isset($id)) return FALSE;
    $terms = $this->term_table;
    $archive = $this->archive_table;
    $archive = $this->archive_table;
    $sql = "WITH a AS (DELETE FROM $terms WHERE id=$id RETURNING *) "
          ."INSERT INTO $archive (user_id_deleted,id,term_e,"
          ."addition,translation_j,pronunciation,explanation,"
          ."category,category_2,synonim_e,synonim_j,antonym_e,"
          ."phrease_group,relatedword,avr_score,text,topic,for_glossary,"
          ."for_glossary2,update,user_id) SELECT $user_id,* FROM a";
    return $this->db->simple_query($sql);
  }

  public function update($data) {
    if ( ! $this->is_valid($data)) return FALSE;
    $term_id = $data["id"];
    $user_id = $this->tank_auth->get_user_id();
    unset($data["id"]);
    unset($data["submit"]);

    $this->db->trans_start();
    $this->delete($term_id, $user_id);
    $this->insert($data, $term_id);
    $this->db->trans_complete();
  }

  function term_id($archive_id) {
    $archive  = $this->archive_table;
    $cond = array("archive_id" => $archive_id);
    $query = $this->db->get_where($archive, $cond);
    return $query->row()->id;
  }

  public function history($term_id=NULL) {
    $cond = array("id"=>$term_id);
    $this->db->select("*")
      ->from($this->archive_table);
    if ($term_id != NULL)
      $this->db->where($cond);
    return $this->db->get();
  }

  public function revert($archive_id) {
    $term_id = $this->term_id($archive_id);
    $archive = $this->archive_table;
    $terms   = $this->term_table;
    $fields = implode(',', $this->fields());

    $sql = "WITH a AS (DELETE FROM $archive WHERE archive_id=$archive_id "
      ."RETURNING $fields) "
      ."INSERT INTO $terms SELECT * from a";
    //    echo $sql;
    $status = $this->db->simple_query($sql);
    return $status ? $term_id : 0;
  }

  public function index($field, $target='') { 
    /*
    return $this->db
      ->order_by($field)
      ->get_where($this->term_table,
		  [$field.' ILIKE', "$target%"]);
    */
    return $this->db->query("SELECT * FROM $this->term_table "
			    ."WHERE $field ILIKE '$target%' "
			    ."ORDER BY $field");
  }

  public function fields() { 
    $fields = $this->db->list_fields($this->term_table);
    return array_reverse($fields);
  }
  
  public function search($keyword="") {
    $like = function($str) use ($keyword) {
      return "$str ::TEXT LIKE '%$keyword%'";
    };
    $fields = $this->fields();
    array_shift($fields);
    $condition = implode(' OR ', array_map($like, $fields));

    $query = $this->db->get_where($this->term_table, $condition);
    return $query;
  }

  public function glossary2_summary () {
    $items = 'for_glossary2,for_glossary';
    $table = $this->term_table;

    return $this->db->query(<<<EOT
      SELECT for_glossary2, count, count2
        FROM (
	 SELECT for_glossary2, count(*)
	   FROM $table
          WHERE for_glossary ~ '主要語'
	  GROUP BY for_glossary2) a
	JOIN (
	 SELECT for_glossary2, count(*) AS count2
	   FROM $table
	  WHERE for_glossary !~ '主要語'
	  GROUP BY for_glossary2) b
	USING (for_glossary2)
      ORDER BY for_glossary2;
EOT
    );
    /*
    $this->db
      ->select($items)
      ->select("count(*)")
      ->from($this->term_table)
      ->where('for_glossary2 !=', '')
      ->group_by($items)
      ->order_by($items);
    return $this->db->get();
    */
  }

  public function get_choice($field) {
    $query=$this->db->query("SELECT DISTINCT $field FROM "
			    .$this->term_table
			    ." ORDER BY $field");
    $val = function($ary) use ($field) { return $ary[$field]; };
    $aray = array_map($val, $query->result_array());
    $ret = array();
    foreach ($aray as $a) {
      $ret[$a] = $a;
    }
    return $ret;
  }

  public function recent($num=20) {
    $this->db->from($this->term_table)
      ->select("*")
      ->order_by("update","DESC")
      ->limit($num);
    return $this->db->get();
  }

  public function is_active_user($user_id) {
    $this->db->select("activated");
    $query = $this->db->get_where("users",array("id"=>$user_id));
    return $query->result()[0]->activated;
  }

}

/* End of class Mdb */
