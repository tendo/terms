<?php
class Mcomment extends CI_Model {
  var $comment_table    = 'comments';
  var $archive_table    = 'comment_archive';
  var $user_table       = 'users';

  function __construct()
  //  function Mdb()
  {
    parent::__construct();
    $this->load->database();
  }

  
  public function get($field, $item) {
    return $this->db->get_where('ONLY '. $this->comment_table,
				 array($field => $item));
  }

  public function card($term_id) { 
    $comment =$this->comment_table;
    $user = $this->user_table;
    $sql = "SELECT $comment.id as comment_id, "
      . "rawdata_id as term_id, user_id, username, update, comment "
      . "FROM ONLY $comment JOIN $user ON user_id=$user.id "
      . "WHERE rawdata_id = $term_id";
    return $this->db->query($sql);
  }

  public function list_deleted($term_id) {
    $cond = array('rawdata_id'=> $term_id);
    return $this->db->get_where($this->archive_table, $cond);
  }

  public function find($field, $item){
    return $this->db->get_where($this->comment_table,
				array("$field LIKE", "%$item%"));
  }

  public function index($field, $item) { 
    return $this->db->get_where($this->comment_table,
				array("$field LIKE", "$item%"));
  }

  public function fields() { 
    return array_reverse($this->db->list_fields($this->comment_table));
 }
  
  public function search($keyword) {
    $like = function ($str) { return "$str LIKE '%$keyword%'"; };
    $conndition = implode(' OR ', array_map($like, $this->fields));

    $query = $this->db->get_where('rawdata', $condition);
    return $query;
  }

  public function delete($term_id,$user_id,$comment_id) {
    $comments = $this->comment_table;
    $archive  = $this->archive_table;
    $sql = "WITH a AS (DELETE FROM $comments "
                     ."WHERE id = $comment_id "
                     ."AND user_id = $user_id "
                     ."AND rawdata_id = $term_id "
                     ."RETURNING *) "
          ."INSERT INTO $archive SELECT * FROM a";
    return $this->db->simple_query($sql);
  }

  public function revert($archive_id) {
    $comments = $this->comment_table;
    $archive  = $this->archive_table;
    $fields   = implode(",", $this->fields());
    
    $sql = "WITH a AS (DELETE FROM $archive WHERE archive_id=$archive_id "
      ."RETURNING $fields) INSERT INTO $comments SELECT * FROM a";
    return $this->db->simple_query($sql);
  }

  public function antibot($user_id) {
    $this->db->where("user_id", $user_id);
    $this->db->where("update > current_timestamp - interval '1 hour'");
    $counts = $this->db->count_all_results('comments');
    return $counts > 10;
  }

  public function add($term_id, $user_id, $comment) {
    // check for valid parameters.
    if ($term_id == "" or $comment == "") return FALSE;

    // check if user exists and active.
    $this->db->where("id", $user_id);
    $user_exists = $this->db->count_all_results($this->user_table);
    $this->load->model('mdb');
    $activated = $this->Mdb->is_user_activated($user_id);
    if ($user_exists !== 1 or $activated === 0) return FALSE;

    // antibot
    if ($user_id === 0) return FALSE;
    if ($this->antibot($user_id)) return FALSE;

    // insert new data
    $data = array(
		  'rawdata_id' => $term_id,
		  'user_id'    => $user_id,
		  'comment'    => $comment,
		  );
    return $this->db->insert($this->comment_table, $data);
  }

  public function stat_by_user_date() {
    return $this->db->query("select update::DATE, user_id, username, count(*) from comments join users on users.id=user_id group by update::DATE,user_id, username order by update desc");

    $this->db->select('update::DATE',FALSE);
    $this->db->select('user_id');
    //$this->db->select('username, email, activated');
    $this->db->select('count(*)');
    $this->db->from('comments');
    $this->db->group_by('update::DATE,user_id',FALSE);
    //    $this->db->join('users', 'users.id=user_id');
    //    $this->db->group_by('update, user_id, username, email, activated');
    $this->db->order_by('update', 'desc');
    return $this->db->get();
  }

  public function recent0($num=20) {
    $this->db->select('*')
      ->order_by('update', 'DESC')
      ->limit($num);
    return $this->db->get();
  }

  public function recent() {
    return $this->db->query("select update::DATE, user_id, username, substring(comment from 1 for 200) from comments join users on users.id=user_id order by update desc");
  
    $this->db->select('update::DATE',FALSE);
    $this->db->select('user_id as ID, Username');
    $this->db->select('substring(comment from 1 for 120) AS comment');
    $this->db->from('comments');
    $this->db->join('users', 'users.id=user_id');
    $this->db->group_by('update,user_id,username');
    $this->db->order_by('update', 'desc');
    $this->db->limit(300);
    return $this->db->get();
  }
}

/* Endo of class Mdb */
