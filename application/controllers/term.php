<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Term extends CI_Controller {

  var $tag = array(
		   "id"=>"ID",
		   "term_e"=>"英用語",
		   "addition"=>"属性",
		   "translation_j"=>"訳語",
		   "pronunciation"=>"発音",
		   "explanation"=>"説明",
		   "category"=>"重要度",
		   "category_2"=>"分野",
		   "synonim_e"=>"同義語（英）",
		   "synonim_j"=>"同義語",
		   "antonym_e"=>"反意語（英）",
		   "phrease_group"=>"語群",
		   "relatedword"=>"関連語",
		   "avr_score"=>"スコア",
		   "text"=>"コメント",
		   "topic"=>"トピック名",
		   "for_glossary"=>"用語集",
		   "for_glossary2"=>"用語項目",
		   "update" => "最終更新",
		   "user_id" => "更新ユーザ",
		   );

  public function __construct()
  {
    parent::__construct();
    header('Content-Type: text/html; charset=utf-8');
    $this->load->model('Mdb');
    $this->load->model('Mcomment');
  }

  public function index()
  {
    redirect(site_url(),'refresh');
  }

  private function view($page, $param="", $subtitle='用語情報')
  {
    $param["user_id"] =  $this->tank_auth->get_user_id();
    $param["tag"] = $this->tag;
    $p['contents'] = array($this->load->view($page, $param, TRUE));
    $p['subtitle'] = $subtitle;
    $this->load->view('template', $p);
  }

  private function message($text="(メッセージ)", $second=3, $param=NULL) {
    $this->load->library('user_agent');

    $param["text"]  = $text;
    $param["second"] = $second;
    $param["referrer"] = $this->agent->referrer();
    $this->view('vmessage', $param);
  }

  public function external_reference($term_id) {
    $ajax = '$(function(){
      var ref="http://genetics.ibio.jp/terms/bin/view/Main/GeneticsTerm";
      $("#reference").get(ref+$term_id);
    });';
    return $ajax;
  }

  private function get_comment($text) {
    $this->load->helper('url');
    $comment_mark = '%COMMENT{type="below"}%';
    $text = preg_replace("/\n\n/", "\n", $text);
    $text = substr($text,strpos($text,$comment_mark)+strlen($comment_mark)+1);
    return auto_link($text);
  }

  public function detail($query) {
    $term_id = $query->row()->id;
    $param["query"] = $query;
    $param["text"] = $this->get_comment($query->row()->text); 
    $param['comments'] = $this->Mcomment->card($query->result()[0]->id);
    $param["deleted_comments"] = $this->Mcomment->list_deleted($term_id);
    $param["history"] = $this->Mdb->history($term_id);
    $param["script"] = $this->external_reference($query->row()->topic);
    $this->view('vdetail', $param);
  }

  function result($query, $title=FALSE) {
    $param["query"] = $query;
    if ($query->num_rows()==1) {
      $this->detail($query);
    } else {
      $this->view('vlist_terms', $param, $title ? $title : '用語リスト');
    }
  }

  public function get($field, $item="test") {
    $title=FALSE;
    $item = rawurldecode($item);
    if ($field == 'user') {
      $field = 'user_id';
      $title = 'あなたが登録した用語';
    } else {
      $title = 'カテゴリ: '.$item;
    }
    $this->result( $this->Mdb->get($field, $item), $title );
  }

  public function find($field, $item="") {
    if($item === "") {
      $this->message("Direct access not allowed");
      return;
    }
    $item = rawurldecode($item);
    $this->result( $this->Mdb->find($field, $item), "検索結果(OR検索):".$item);
  }

  public function search() {
    $keyword = $this->input->post('keyword');
    $this->result( $this->Mdb->search($keyword), "検索結果: ".$keyword);
  }

  public function listing($field) {
    $this->result( $this->Mdb->listing($field),
		   '「'.$this->tag[$field].'」に記入がある語');
  }

  public function register() {
    $this->view('vregistered');
  }

  public function confirm_delete($data) {
    $this->view('vconfirm_delete',$data);
  }

  public function backup($id) {
    $archive = "rawdata_archive";
    $query = $this->db->get_where($archive, array('id' => $id));
    if ($query->num_rows() >0 ) {
      $param["result"] = $this->result();
      $this->view("vbackup", $param);
    } else {
      $this->message("更新・削除されたデータはありません");
    }
  }

  public function deleted() {
    if ($this->tank_auth->is_logged_in()) {
      $user_id = $this->tank_auth->get_user_id();
      $id = $this->input->post("id");
      if ($this->Mdb->delete($id, $user_id)) {
	$this->message('削除しました  ID:'.$id);
	return;
      }
    }
    $this->message('削除できません');
  }

  public function update() {
    if ($this->tank_auth->is_logged_in()) {
      $data = $this->input->post(NULL, TRUE);
      if ( $this->input->post('delete') ) 
	$this->confirm_delete($data);
      else if ($data) {
	$res = $this->Mdb->update($data);
	redirect(site_url('term/get/id/'.$data["id"]));
      } else {
	$this->view('vunder_construction','',"用語登録");
	redirect(site_url(''));
      }
    } else {
      $this->message('編集・削除にはログインが必要です');
    }
  }

  public function revert($archive_id) {
    $term_id = $this->Mdb->revert($archive_id);
    if ($term_id > 0) {
      $this->message("用語ID ".$term_id."を復元しました");
    } else {
      $this->message("復活できませんでした");
    }
  }
}

/* End of file term.php */
/* Location: ./application/controllers/term.php */