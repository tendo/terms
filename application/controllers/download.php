<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mdb');
  }

  public function index()
  {
    //$this->home();
    $this->all();
  }

  private function view($page, $param="", $subtitle="")
  {
    $p['subtitle'] = $subtitle;
    $p['contents'] = array($this->load->view($page, $param, TRUE),
			   );
    $this->load->view('template', $p);
  }

  public function home()
  {
    $this->view('vdownload');
  }

  public function all( $delimiter = ",", $newline = "\n" )
  { 
    $this->load->dbutil();

    $columns = array( "r.id",
		      "term_e",
		      "addition",
		      "translation_j",
		      "pronunciation",
		      "explanation",
		      "category",
		      "category_2",
		      "synonim_e",
		      "synonim_j",
		      "antonym_e",
		      "phrease_group",
		      "relatedword",
		      "avr_score",
		      //"text",
		      "topic",
		      "for_glossary",
		      "for_glossary2",
		      "update",
		      //"user_id",
		      "username",
		      );

    // get joined comment list.
    $comments = "SELECT comment || '\r\n -- ' || username || ' at ' || update"
               ." FROM comments c"
               ." JOIN users cu ON cu.id=c.user_id"
               ." WHERE rawdata_id=r.id";
    $array = "ARRAY_TO_STRING(ARRAY($comments), '\r\n') AS comments";
    $fields = implode(",", $columns);
    $query = $this->db->query("SELECT $fields, $array FROM rawdata r"
			      ." JOIN users u ON u.id=user_id"
			      ." ORDER BY r.id");
    //    $query = $this->db->query("SELECT $fields FROM rawdata ORDER BY id");

    $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
    $name = "yougo.csv";

    // echo mb_convert_encoding($data, "SJIS", "UTF-8");
    // return;
    $this->output->set_content_type('test/csv')
      ->set_header("Content-Disposition: attachment; filename='{$name}'")
      ->set_output(mb_convert_encoding($data, "SJIS", "UTF-8"));
  }
}

/* End of file download.php */
/* Location: ./application/controllers/download.php */