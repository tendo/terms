<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Policy extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		header("Content-type: text/html; charset=UTF-8");
		$this->load->view('vpolicy_utf');
	}

	function about()
	{
		header("Content-type: application/pdf;");
		$this->load->view('masuya2010.pdf');
	}
}

/* End of file policy.php */
/* Location: ./application/controllers/policy.php */
