<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mdb');
  }

  public function index()
  {
    $this->home();
  }

  private function view($page, $param="", $subtitle="")
  {
    $p['subtitle'] = $subtitle;
    $p['contents'] = array($this->load->view($page, $param, TRUE),
			   );
    $this->load->view('template', $p);
  }

  public function home()
  {
    $this->view('vtop');
  }

  public function list_terms($head='')
  { 
    $param["query"] = $this->Mdb->index('term_e', $head);
    $this->view('vlist_terms', $param, "用語リスト: ".$head);
  }

  public function recent($num=20)
  { 
    // obtain recent updates from term table
    $param["query"] = $this->Mdb->recent($num);
    $this->view('vlist_terms', $param, '最近の更新 ('.$num.')');
  }

  public function new_term()
  {
    redirect(site_url('newterm'));
  }

  public function search($term='') {
    if ($term==="") {
      $term=$this->input->post('keyword');
    }
    $res = $this->Mdb->search($term);
    if ( $res->num_rows() == 1) {
      redirect(site_url('term/detail'.$res->result()[0]->id));
    } else {
      $this->list($res);
    }
  }

  public function deleted_term() {
    $param["history"] = $this->Mdb->history();
    $this->view('vhistory',$param, "削除・上書きされた用語");
  }

  public function glossary2_summary() {
    $param["query"] = $this->Mdb->glossary2_summary();
    $this->view('vglossary2', $param, "詳解語");
  }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */