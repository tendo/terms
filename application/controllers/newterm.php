<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newterm extends CI_Controller {
  var $chooser = array(
		       "category",
		       "phrease_group",
		       "for_glossary",
		       "for_glossary2"
		       );
  var $tag = array(
		   "id"=>"ID",
		   "term_e"=>"英用語",
		   "addition"=>"属性",
		   "translation_j"=>"訳語",
		   "pronunciation"=>"発音",
		   "explanation"=>"説明",
		   "category"=>"重要度",
		   "category_2"=>"分野",
		   "synonim_e"=>"同義語（英）",
		   "synonim_j"=>"同義語",
		   "antonym_e"=>"反意語（英）",
		   "phrease_group"=>"語群",
		   "relatedword"=>"関連語",
		   "avr_score"=>"ｽｺｱ",
		   "text"=>"備考",
		   "topic"=>"トピック名",
		   "for_glossary"=>"用語集",
		   "for_glossary2"=>"用語項目",
		   "update" => "最終更新",
		   "user_id" => "更新ユーザ",
		   );

  public function __construct()
  {
    parent::__construct();
    header('Content-Type: text/html; charset=utf-8');
    $this->load->model('Mdb');
    $this->load->helper(array('form','url'));
    $this->load->library('form_validation');
    $this->load->library('tank_auth');
  }

  private function view($page, $param="",$subtitle="新規登録")
  {
    $p['subtitle'] = $subtitle;
    $p['contents'] = array($this->load->view($page, $param, TRUE));
    $this->load->view('template', $p);
  }

  private function message($text="(メッセージ)", $second=3, $param=NULL) {
    $param["text"]  = $text;
    $param["second"] = $second; // time duration for display
    $this->view('vmessage', $param);
  }

  public function index()
  {
    if ( ! $this->tank_auth->is_logged_in()){
      $p["contents"][0] = $this->load->view('vneed_login','',TRUE);
      $p["subtitle"]='登録・編集にはログイン（登録）が必要です。';
      $this->load->view('template', $p);
    } else if ($this->form_validation->run() == TRUE) {
      $this->register();
    } 
else {
      $param["fields"] = $this->Mdb->fields();
      $param["tag"] = $this->tag;
      foreach ($this->chooser as $field) {
	$param["chooser"][$field] = $this->Mdb->get_choice($field);
      }
      $this->view('vnewterm', $param,"新規登録");
    }
  }  

  // 新語登録
  public function add() {
    $data = $this->input->post(NULL, TRUE);
    unset($data["submit"]);

    if ($data["term_e"] === '') {
      $param["data"] = $data;
      $this->message('term_eが空白では登録できません。', 3, $param);
      return;
    }
    $result = $this->Mdb->insert($data);
    if ($result > 0) {
      $param["term"]=$data["term_e"]; 
      $this->message('登録しました');
    } else {
      $param["data"]=$data;
      $this->message('登録できませんでした', 3, $param);
    }
  }
}

/* End of file newterm.php */
/* Location: ./application/controllers/newterm.php */
