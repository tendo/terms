<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    header('Content-Type: text/html; charset=utf-8');
    $this->load->model('Mdb');
    $this->load->model('Mcomment');
  }

  private function view($page, $param="", $subtitle="")
  {
    //    $p['title'] = '日本遺伝学会　遺伝学用語集編纂プロジェクト';
    //    $p['buttons'] = $this->load->view('vbuttons', '', TRUE);
    //    $p['footer'] = $this->load->view('vbuttons', '', TRUE);
    $p['subtitle'] = $subtitle;
    $p['contents'] = array($this->load->view($page, $param, TRUE));
    $this->load->view('template', $p);
  }

  public function search($term='') {
    if ($term==="") {
      $term=$this->input->post('keyword');
    }
    // find user
  }

  public function add () {
    $this->input->post('username');
    $this->Mdb->add_user();
  }
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
