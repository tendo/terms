<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    header('Content-Type: text/html; charset=utf-8');
    $this->load->library('table');
    $this->load->model('Mdb');
    $this->load->model('Mcomment');
  }

  private function view($page, $param="", $subtitle="コメント")
  {
    $p['subtitle'] = $subtitle;
    $p['contents'] = array($this->load->view($page, $param, TRUE));
    $this->load->view('template', $p);
  }

  private function message($text="(メッセージ)", $second=3, $param=NULL) {
    $param["text"]  = $text;
    $param["second"] = $second;
    $this->view('vmessage', $param);
  }

  public function user_id() {
    return $this->tank_auth->get_user_id();
  }

  function sendmail($term_id, $user_id, $comment) {
    $this->email->from('genetics_terms@ibio.jp');
    $this->email->to('genetics_terms@ibio.jp');
    $this->email->subject('A comment added by '.$user_id);
    $this->email->message('Term_id: '.$term_id.'\n'.$comment );
    $this->email->send();
  }

  public function add()
  {
    $user_id = $this->user_id();
    $term_id = $this->input->post('term_id');
    $comment = $this->input->post('comment');

    if ( ! isset($user_id)) return FALSE;
    if ($user_id < 1) return FALSE;

    $result = $this->Mcomment->add($term_id, $user_id, $comment);
    if ($result == TRUE) {
      $this->sendmail($term_id,$user_id,$comment);
      redirect(site_url("term/get/id/$term_id"));
    }  else {
      $p["contents"] = "term: $term_id, user: $user_id"
	."<br/>comment: $comment";
      $this->view('vneed_login', $p);
    }
  }

  public function delete() {
    $term_id = $this->input->post('term_id');
    $user_id = $this->input->post('user_id');
    $comment_id = $this->input->post('comment_id');
    if ($user_id == $this->user_id()){
      $status = $this->Mcomment->delete($term_id,$user_id,$comment_id);
    } else {
      $this->message("登録者しか削除できません");
    }
    if ($status) {
      $this->message("削除しました: $term_id $user_id $comment_id");
    } else {
      $this->message("削除できません: $term_id $user_id $comment_id");
    }
  }

  public function list_deleted($term_id) {
    $query = $this->Mcomment->list_deleted($term_id);
    if ( $query->num_rows() ) {
      $param["result"] = $query->result();
      $title = "削除されたコメント (用語ID ".$term_id. ')';
      $this->view('vlist_deleted_comments', $param, $title);
    } else {
      $this->message("削除されたコメントはありません");
    }
  }

  public function revert ($archive_id) {
    $status = $this->Mcomment->revert($archive_id);
    if ($status) {
      $this->message("復元しました");
    } else {
      $this->message("復元できません");
    }
  }

  function reporting($query) {
    header('Refresh: 60;');
    $this->load->helper('date');
    echo standard_date('DATE_ATOM', time());;
    echo $this->table->generate($query);
  }

  public function stat() {
    $query = $this->Mcomment->stat_by_user_date();
    $this->reporting($query);
  }

  public function recent() {
    $query = $this->Mcomment->recent();
    $this->reporting($query);
 }


}

/* End of file comment.php */
/* Location: ./application/controllers/comment.php */
