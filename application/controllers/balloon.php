<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Balloon extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    header('Content-Type: text/html; charset=utf-8');
    $this->load->model('Mdb');
    $this->load->model('Mcomment');
  }


  public function index() {
    $this->load->view('vballoon');
  }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */