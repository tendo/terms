<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    
    $this->load->helper('url');
    $this->load->library('tank_auth');
  }
  
  private function view($page, $param="", $subtitle="")
  {
    $p['subtitle'] = $subtitle;
    $p['contents'] = array($this->load->view($page, $param, TRUE));
    $this->load->view('template', $p);
  }

  function index()
  {
    $this->view("vlogout");
  }
}

/* End of file logout.php */
/* Location: ./application/controllers/logout.php */
