<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tab extends CI_Controller {

	public function index()
	{
		$this->load->view('vtab');
	}
}

/* End of file tab.php */
/* Location: ./application/controllers/tab.php */