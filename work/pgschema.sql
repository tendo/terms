-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS ci_sessions (
  session_id varchar(40) NOT NULL DEFAULT '0',
  ip_address varchar(16) NOT NULL DEFAULT '0',
  user_agent varchar(150) NOT NULL,
  last_activity INT NOT NULL DEFAULT '0',
  user_data text NOT NULL,
  PRIMARY KEY (session_id)
);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS login_attempts (
  id SERIAL PRIMARY KEY,
  ip_address varchar(40) NOT NULL,
  login varchar(50) NOT NULL,
  "time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP -- ON UPDATE CURRENT_TIMESTAMP
);

CREATE OR REPLACE FUNCTION update_time_column()
RETURNS TRIGGER AS $$
BEGIN
   NEW.time = now();
   RETURN NEW;
END
$$ language 'plpgsql';

CREATE TRIGGER update_login_attempts_update_column BEFORE UPDATE
ON login_attempts FOR EACH ROW EXECUTE PROCEDURE
update_time_column();


-- --------------------------------------------------------

--
-- Table structure for table `user_autologin`
--

CREATE TABLE IF NOT EXISTS user_autologin (
  key_id char(32) NOT NULL,
  user_id int NOT NULL DEFAULT '0',
  user_agent varchar(150) NOT NULL,
  last_ip varchar(40) NOT NULL,
  last_login timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, -- ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (key_id,user_id)
);

CREATE OR REPLACE FUNCTION update_last_login_column()
RETURNS TRIGGER AS $$
BEGIN
   NEW.last_login = now();
   RETURN NEW;
END
$$ language 'plpgsql';

CREATE TRIGGER update_user_autologin_last_login_column BEFORE UPDATE
ON user_autologin FOR EACH ROW EXECUTE PROCEDURE
update_last_login_column();

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS user_profiles (
  id SERIAL PRIMARY KEY,
  user_id int NOT NULL,
  country varchar(20) DEFAULT NULL,
  website varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS users (
  id SERIAL PRIMARY KEY,
  username varchar(50) NOT NULL,
  password varchar(255) NOT NULL,
  email varchar(100) NOT NULL,
--  activated tinyint NOT NULL DEFAULT '1',
--  banned tinyint NOT NULL DEFAULT '0'
  activated smallint NOT NULL DEFAULT '1',
  banned smallint NOT NULL DEFAULT '0',
  ban_reason varchar(255) DEFAULT NULL,
  new_password_key varchar(50) DEFAULT NULL,
--  new_password_requested datetime DEFAULT NULL,
  new_password_requested timestamp DEFAULT NULL,
  new_email varchar(100) DEFAULT NULL,
  new_email_key varchar(50) DEFAULT NULL,
  last_ip varchar(40) NOT NULL,
--  last_login datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
--  created datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  last_login timestamp,
  created timestamp,
  modified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP -- ON UPDATE CURRENT_TIMESTAMP,
--  PRIMARY KEY (id)
);

CREATE OR REPLACE FUNCTION update_modified_column()
RETURNS TRIGGER AS $$
BEGIN
   NEW.modified = now();
   RETURN NEW;
END
$$ language 'plpgsql';

CREATE TRIGGER update_users_modified_column BEFORE UPDATE
ON users FOR EACH ROW EXECUTE PROCEDURE
update_modified_column();
