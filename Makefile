

.PHONY: db_prep

PG_ADMIN := postgres

db_prep:
	createuser -U $(PG_ADMIN) genetics
	psql -U $(PG_ADMIN) <genetics.sql
