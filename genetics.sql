--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.24
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: backup; Type: SCHEMA; Schema: -; Owner: genetics
--

CREATE SCHEMA backup;


ALTER SCHEMA backup OWNER TO genetics;

--
-- Name: genet; Type: SCHEMA; Schema: -; Owner: genetics
--

CREATE SCHEMA genet;


ALTER SCHEMA genet OWNER TO genetics;

--
-- Name: genetics; Type: SCHEMA; Schema: -; Owner: genetics
--

CREATE SCHEMA genetics;


ALTER SCHEMA genetics OWNER TO genetics;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


SET search_path = public, pg_catalog;

--
-- Name: update_last_login_column(); Type: FUNCTION; Schema: public; Owner: genetics
--

CREATE FUNCTION update_last_login_column() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.last_login = now();
   RETURN NEW;
END
$$;


ALTER FUNCTION public.update_last_login_column() OWNER TO genetics;

--
-- Name: update_modified_column(); Type: FUNCTION; Schema: public; Owner: genetics
--

CREATE FUNCTION update_modified_column() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.modified = now();
   RETURN NEW;
END
$$;


ALTER FUNCTION public.update_modified_column() OWNER TO genetics;

--
-- Name: update_time_column(); Type: FUNCTION; Schema: public; Owner: genetics
--

CREATE FUNCTION update_time_column() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.time = now();
   RETURN NEW;
END
$$;


ALTER FUNCTION public.update_time_column() OWNER TO genetics;

SET search_path = backup, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: rawdata; Type: TABLE; Schema: backup; Owner: genetics
--

CREATE TABLE rawdata (
    id integer,
    term_e text,
    addition text,
    translation_j text,
    pronunciation text,
    explanation text,
    category text,
    category_2 text,
    synonim_e text,
    synonim_j text,
    antonym_e text,
    phrease_group text,
    relatedword text,
    avr_score text,
    text text,
    topic text,
    for_glossary text,
    for_glossary2 text,
    update timestamp without time zone,
    user_id integer
);


ALTER TABLE rawdata OWNER TO genetics;

--
-- Name: rawdata_archive; Type: TABLE; Schema: backup; Owner: genetics
--

CREATE TABLE rawdata_archive (
    archive_id integer,
    id integer,
    term_e text,
    addition text,
    translation_j text,
    pronunciation text,
    explanation text,
    category text,
    category_2 text,
    synonim_e text,
    synonim_j text,
    antonym_e text,
    phrease_group text,
    relatedword text,
    avr_score text,
    text text,
    topic text,
    for_glossary text,
    for_glossary2 text,
    deleted timestamp without time zone,
    update timestamp without time zone,
    user_id integer,
    user_id_deleted integer
);


ALTER TABLE rawdata_archive OWNER TO genetics;

SET search_path = genet, pg_catalog;

--
-- Name: article; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE article (
    article_id integer NOT NULL,
    atype_id integer,
    acontent text,
    jterm_id integer
);


ALTER TABLE article OWNER TO genetics;

--
-- Name: article_article_id_seq; Type: SEQUENCE; Schema: genet; Owner: genetics
--

CREATE SEQUENCE article_article_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE article_article_id_seq OWNER TO genetics;

--
-- Name: article_article_id_seq; Type: SEQUENCE OWNED BY; Schema: genet; Owner: genetics
--

ALTER SEQUENCE article_article_id_seq OWNED BY article.article_id;


--
-- Name: article_type; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE article_type (
    atype_id integer NOT NULL,
    alength integer,
    anote text
);


ALTER TABLE article_type OWNER TO genetics;

--
-- Name: article_type_atype_id_seq; Type: SEQUENCE; Schema: genet; Owner: genetics
--

CREATE SEQUENCE article_type_atype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE article_type_atype_id_seq OWNER TO genetics;

--
-- Name: article_type_atype_id_seq; Type: SEQUENCE OWNED BY; Schema: genet; Owner: genetics
--

ALTER SEQUENCE article_type_atype_id_seq OWNED BY article_type.atype_id;


--
-- Name: dicipline; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE dicipline (
    dicipline_id integer NOT NULL,
    dicipline text,
    dupdate timestamp without time zone DEFAULT '2012-07-06 19:45:44.265645'::timestamp without time zone,
    dupdateuser integer
);


ALTER TABLE dicipline OWNER TO genetics;

--
-- Name: dicipline_dicipline_id_seq; Type: SEQUENCE; Schema: genet; Owner: genetics
--

CREATE SEQUENCE dicipline_dicipline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dicipline_dicipline_id_seq OWNER TO genetics;

--
-- Name: dicipline_dicipline_id_seq; Type: SEQUENCE OWNED BY; Schema: genet; Owner: genetics
--

ALTER SEQUENCE dicipline_dicipline_id_seq OWNED BY dicipline.dicipline_id;


--
-- Name: e_antonym; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE e_antonym (
    eterm_id integer,
    eantonym integer[]
);


ALTER TABLE e_antonym OWNER TO genetics;

--
-- Name: e_synonym; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE e_synonym (
    eterm_id integer[]
);


ALTER TABLE e_synonym OWNER TO genetics;

--
-- Name: e_term; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE e_term (
    eterm_id integer NOT NULL,
    eterm text,
    epronunciation text,
    enote text,
    eupdate timestamp without time zone DEFAULT '2012-07-06 19:51:12.905916'::timestamp without time zone,
    eupdateuser integer
);


ALTER TABLE e_term OWNER TO genetics;

--
-- Name: e_term_eterm_id_seq; Type: SEQUENCE; Schema: genet; Owner: genetics
--

CREATE SEQUENCE e_term_eterm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE e_term_eterm_id_seq OWNER TO genetics;

--
-- Name: e_term_eterm_id_seq; Type: SEQUENCE OWNED BY; Schema: genet; Owner: genetics
--

ALTER SEQUENCE e_term_eterm_id_seq OWNED BY e_term.eterm_id;


--
-- Name: j_synonym; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE j_synonym (
    jterm_id integer[]
);


ALTER TABLE j_synonym OWNER TO genetics;

--
-- Name: j_term; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE j_term (
    jterm_id integer NOT NULL,
    jterm text,
    jnote text,
    dicipline_id integer,
    jupdate timestamp without time zone DEFAULT '2012-07-06 19:46:45.655671'::timestamp without time zone,
    jupdateuser integer
);


ALTER TABLE j_term OWNER TO genetics;

--
-- Name: j_term_jterm_id_seq; Type: SEQUENCE; Schema: genet; Owner: genetics
--

CREATE SEQUENCE j_term_jterm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE j_term_jterm_id_seq OWNER TO genetics;

--
-- Name: j_term_jterm_id_seq; Type: SEQUENCE OWNED BY; Schema: genet; Owner: genetics
--

ALTER SEQUENCE j_term_jterm_id_seq OWNED BY j_term.jterm_id;


--
-- Name: jterm_eterm_link; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE jterm_eterm_link (
    jterm_id integer,
    eterm_id integer
);


ALTER TABLE jterm_eterm_link OWNER TO genetics;

--
-- Name: phrase_group; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE phrase_group (
    pgtitle text NOT NULL,
    jterm_ids integer[],
    pgupdate timestamp without time zone DEFAULT '2012-07-06 20:45:00.916079'::timestamp without time zone,
    pgupdateuser integer
);


ALTER TABLE phrase_group OWNER TO genetics;

--
-- Name: priority; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE priority (
    jterm_id integer,
    plevel integer,
    pudate timestamp without time zone DEFAULT '2012-07-06 20:00:06.356491'::timestamp without time zone,
    pupdateuser integer
);


ALTER TABLE priority OWNER TO genetics;

--
-- Name: priority_levels; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE priority_levels (
    plevel integer NOT NULL,
    ptext text NOT NULL,
    plupdate timestamp without time zone DEFAULT '2012-07-06 20:00:01.496098'::timestamp without time zone,
    plupdateuser integer
);


ALTER TABLE priority_levels OWNER TO genetics;

--
-- Name: rating; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE rating (
    jterm_id integer,
    rate double precision
);


ALTER TABLE rating OWNER TO genetics;

--
-- Name: related_terms; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE related_terms (
    jterm_id integer,
    relterms integer[],
    relupdate timestamp without time zone DEFAULT '2012-07-06 20:46:52.99608'::timestamp without time zone,
    relupdateuser integer
);


ALTER TABLE related_terms OWNER TO genetics;

--
-- Name: usernames; Type: TABLE; Schema: genet; Owner: genetics
--

CREATE TABLE usernames (
    user_id integer NOT NULL,
    username text,
    unote text,
    uupdate timestamp without time zone DEFAULT '2012-07-06 19:33:56.386128'::timestamp without time zone,
    uupdateuser integer
);


ALTER TABLE usernames OWNER TO genetics;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: genet; Owner: genetics
--

CREATE SEQUENCE users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_user_id_seq OWNER TO genetics;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: genet; Owner: genetics
--

ALTER SEQUENCE users_user_id_seq OWNED BY usernames.user_id;


SET search_path = genetics, pg_catalog;

--
-- Name: comments1; Type: TABLE; Schema: genetics; Owner: genetics
--

CREATE TABLE comments1 (
    id integer,
    rawdata_id integer,
    user_id integer,
    update timestamp without time zone,
    comment text
);


ALTER TABLE comments1 OWNER TO genetics;

--
-- Name: comments2; Type: TABLE; Schema: genetics; Owner: genetics
--

CREATE TABLE comments2 (
    id integer,
    rawdata_id integer,
    user_id integer,
    update timestamp without time zone,
    comment text
);


ALTER TABLE comments2 OWNER TO genetics;

SET search_path = public, pg_catalog;

--
-- Name: ci_sessions; Type: TABLE; Schema: public; Owner: genetics
--

CREATE TABLE ci_sessions (
    session_id character varying(40) DEFAULT '0'::character varying NOT NULL,
    ip_address character varying(16) DEFAULT '0'::character varying NOT NULL,
    user_agent character varying(150) NOT NULL,
    last_activity integer DEFAULT 0 NOT NULL,
    user_data text NOT NULL
);


ALTER TABLE ci_sessions OWNER TO genetics;

--
-- Name: comments; Type: TABLE; Schema: public; Owner: genetics
--

CREATE TABLE comments (
    id integer NOT NULL,
    rawdata_id integer NOT NULL,
    user_id integer NOT NULL,
    update timestamp without time zone DEFAULT now(),
    comment text
);


ALTER TABLE comments OWNER TO genetics;

--
-- Name: comment_archive; Type: TABLE; Schema: public; Owner: genetics
--

CREATE TABLE comment_archive (
    archive_id integer NOT NULL,
    deleted timestamp without time zone DEFAULT now()
)
INHERITS (comments);


ALTER TABLE comment_archive OWNER TO genetics;

--
-- Name: comment_archive_archive_id_seq; Type: SEQUENCE; Schema: public; Owner: genetics
--

CREATE SEQUENCE comment_archive_archive_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE comment_archive_archive_id_seq OWNER TO genetics;

--
-- Name: comment_archive_archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: genetics
--

ALTER SEQUENCE comment_archive_archive_id_seq OWNED BY comment_archive.archive_id;


--
-- Name: comment_id_seq; Type: SEQUENCE; Schema: public; Owner: genetics
--

CREATE SEQUENCE comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE comment_id_seq OWNER TO genetics;

--
-- Name: comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: genetics
--

ALTER SEQUENCE comment_id_seq OWNED BY comments.id;


--
-- Name: login_attempts; Type: TABLE; Schema: public; Owner: genetics
--

CREATE TABLE login_attempts (
    id integer NOT NULL,
    ip_address character varying(40) NOT NULL,
    login character varying(50) NOT NULL,
    "time" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE login_attempts OWNER TO genetics;

--
-- Name: login_attempts_id_seq; Type: SEQUENCE; Schema: public; Owner: genetics
--

CREATE SEQUENCE login_attempts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE login_attempts_id_seq OWNER TO genetics;

--
-- Name: login_attempts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: genetics
--

ALTER SEQUENCE login_attempts_id_seq OWNED BY login_attempts.id;


--
-- Name: rawdata_id_seq; Type: SEQUENCE; Schema: public; Owner: genetics
--

CREATE SEQUENCE rawdata_id_seq
    START WITH 2819
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rawdata_id_seq OWNER TO genetics;

--
-- Name: rawdata; Type: TABLE; Schema: public; Owner: genetics
--

CREATE TABLE rawdata (
    id integer DEFAULT nextval('rawdata_id_seq'::regclass) NOT NULL,
    term_e text NOT NULL,
    addition text DEFAULT ''::text,
    translation_j text DEFAULT ''::text,
    pronunciation text DEFAULT ''::text,
    explanation text DEFAULT ''::text,
    category text DEFAULT ''::text,
    category_2 text DEFAULT ''::text,
    synonim_e text DEFAULT ''::text,
    synonim_j text DEFAULT ''::text,
    antonym_e text DEFAULT ''::text,
    phrease_group text DEFAULT ''::text,
    relatedword text DEFAULT ''::text,
    avr_score text DEFAULT ''::text,
    text text DEFAULT ''::text,
    topic text DEFAULT ''::text,
    for_glossary text DEFAULT ''::text,
    for_glossary2 text DEFAULT ''::text,
    update timestamp without time zone DEFAULT now(),
    user_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE rawdata OWNER TO genetics;

--
-- Name: rawdata_archive; Type: TABLE; Schema: public; Owner: genetics
--

CREATE TABLE rawdata_archive (
    archive_id integer NOT NULL,
    id integer,
    term_e text,
    addition text,
    translation_j text,
    pronunciation text,
    explanation text,
    category text,
    category_2 text,
    synonim_e text,
    synonim_j text,
    antonym_e text,
    phrease_group text,
    relatedword text,
    avr_score text,
    text text,
    topic text,
    for_glossary text,
    for_glossary2 text,
    deleted timestamp without time zone DEFAULT now(),
    update timestamp without time zone NOT NULL,
    user_id integer NOT NULL,
    user_id_deleted integer NOT NULL
);


ALTER TABLE rawdata_archive OWNER TO genetics;

--
-- Name: rawdata_archive_id_seq; Type: SEQUENCE; Schema: public; Owner: genetics
--

CREATE SEQUENCE rawdata_archive_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rawdata_archive_id_seq OWNER TO genetics;

--
-- Name: rawdata_archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: genetics
--

ALTER SEQUENCE rawdata_archive_id_seq OWNED BY rawdata_archive.archive_id;


--
-- Name: user_autologin; Type: TABLE; Schema: public; Owner: genetics
--

CREATE TABLE user_autologin (
    key_id character(32) NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    user_agent character varying(150) NOT NULL,
    last_ip character varying(40) NOT NULL,
    last_login timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE user_autologin OWNER TO genetics;

--
-- Name: user_profiles; Type: TABLE; Schema: public; Owner: genetics
--

CREATE TABLE user_profiles (
    id integer NOT NULL,
    user_id integer NOT NULL,
    country character varying(20) DEFAULT NULL::character varying,
    website character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE user_profiles OWNER TO genetics;

--
-- Name: user_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: genetics
--

CREATE SEQUENCE user_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_profiles_id_seq OWNER TO genetics;

--
-- Name: user_profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: genetics
--

ALTER SEQUENCE user_profiles_id_seq OWNED BY user_profiles.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: genetics
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(255) NOT NULL,
    email character varying(100) NOT NULL,
    activated smallint DEFAULT 1::smallint NOT NULL,
    banned smallint DEFAULT 0::smallint NOT NULL,
    ban_reason character varying(255) DEFAULT NULL::character varying,
    new_password_key character varying(50) DEFAULT NULL::character varying,
    new_password_requested timestamp without time zone,
    new_email character varying(100) DEFAULT NULL::character varying,
    new_email_key character varying(50) DEFAULT NULL::character varying,
    last_ip character varying(40) NOT NULL,
    last_login timestamp without time zone,
    created timestamp without time zone,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    affiliation text,
    dicipline text
);


ALTER TABLE users OWNER TO genetics;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: genetics
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO genetics;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: genetics
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


SET search_path = genet, pg_catalog;

--
-- Name: article article_id; Type: DEFAULT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY article ALTER COLUMN article_id SET DEFAULT nextval('article_article_id_seq'::regclass);


--
-- Name: article_type atype_id; Type: DEFAULT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY article_type ALTER COLUMN atype_id SET DEFAULT nextval('article_type_atype_id_seq'::regclass);


--
-- Name: dicipline dicipline_id; Type: DEFAULT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY dicipline ALTER COLUMN dicipline_id SET DEFAULT nextval('dicipline_dicipline_id_seq'::regclass);


--
-- Name: e_term eterm_id; Type: DEFAULT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY e_term ALTER COLUMN eterm_id SET DEFAULT nextval('e_term_eterm_id_seq'::regclass);


--
-- Name: j_term jterm_id; Type: DEFAULT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY j_term ALTER COLUMN jterm_id SET DEFAULT nextval('j_term_jterm_id_seq'::regclass);


--
-- Name: usernames user_id; Type: DEFAULT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY usernames ALTER COLUMN user_id SET DEFAULT nextval('users_user_id_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- Name: comment_archive id; Type: DEFAULT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY comment_archive ALTER COLUMN id SET DEFAULT nextval('comment_id_seq'::regclass);


--
-- Name: comment_archive update; Type: DEFAULT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY comment_archive ALTER COLUMN update SET DEFAULT now();


--
-- Name: comment_archive archive_id; Type: DEFAULT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY comment_archive ALTER COLUMN archive_id SET DEFAULT nextval('comment_archive_archive_id_seq'::regclass);


--
-- Name: comments id; Type: DEFAULT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY comments ALTER COLUMN id SET DEFAULT nextval('comment_id_seq'::regclass);


--
-- Name: login_attempts id; Type: DEFAULT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY login_attempts ALTER COLUMN id SET DEFAULT nextval('login_attempts_id_seq'::regclass);


--
-- Name: rawdata_archive archive_id; Type: DEFAULT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY rawdata_archive ALTER COLUMN archive_id SET DEFAULT nextval('rawdata_archive_id_seq'::regclass);


--
-- Name: user_profiles id; Type: DEFAULT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY user_profiles ALTER COLUMN id SET DEFAULT nextval('user_profiles_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


SET search_path = genet, pg_catalog;

--
-- Name: article article_pkey; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY article
    ADD CONSTRAINT article_pkey PRIMARY KEY (article_id);


--
-- Name: article_type article_type_pkey; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY article_type
    ADD CONSTRAINT article_type_pkey PRIMARY KEY (atype_id);


--
-- Name: dicipline dicipline_dicipline_key; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY dicipline
    ADD CONSTRAINT dicipline_dicipline_key UNIQUE (dicipline);


--
-- Name: dicipline dicipline_pkey; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY dicipline
    ADD CONSTRAINT dicipline_pkey PRIMARY KEY (dicipline_id);


--
-- Name: e_term e_term_eterm_key; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY e_term
    ADD CONSTRAINT e_term_eterm_key UNIQUE (eterm);


--
-- Name: e_term e_term_pkey; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY e_term
    ADD CONSTRAINT e_term_pkey PRIMARY KEY (eterm_id);


--
-- Name: j_term j_term_jterm_key; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY j_term
    ADD CONSTRAINT j_term_jterm_key UNIQUE (jterm);


--
-- Name: j_term j_term_pkey; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY j_term
    ADD CONSTRAINT j_term_pkey PRIMARY KEY (jterm_id);


--
-- Name: phrase_group phrase_group_pkey; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY phrase_group
    ADD CONSTRAINT phrase_group_pkey PRIMARY KEY (pgtitle);


--
-- Name: priority_levels priority_levels_pkey; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY priority_levels
    ADD CONSTRAINT priority_levels_pkey PRIMARY KEY (plevel);


--
-- Name: priority_levels priority_levels_ptext_key; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY priority_levels
    ADD CONSTRAINT priority_levels_ptext_key UNIQUE (ptext);


--
-- Name: usernames users_pkey; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY usernames
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: usernames users_username_key; Type: CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY usernames
    ADD CONSTRAINT users_username_key UNIQUE (username);


SET search_path = public, pg_catalog;

--
-- Name: ci_sessions ci_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY ci_sessions
    ADD CONSTRAINT ci_sessions_pkey PRIMARY KEY (session_id);


--
-- Name: comment_archive comment_archive_pkey; Type: CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY comment_archive
    ADD CONSTRAINT comment_archive_pkey PRIMARY KEY (archive_id);


--
-- Name: comments comment_pkey; Type: CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comment_pkey PRIMARY KEY (id);


--
-- Name: login_attempts login_attempts_pkey; Type: CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY login_attempts
    ADD CONSTRAINT login_attempts_pkey PRIMARY KEY (id);


--
-- Name: rawdata_archive rawdata_archive_pkey; Type: CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY rawdata_archive
    ADD CONSTRAINT rawdata_archive_pkey PRIMARY KEY (archive_id);


--
-- Name: rawdata rawdata_pkey; Type: CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY rawdata
    ADD CONSTRAINT rawdata_pkey PRIMARY KEY (id);


--
-- Name: user_autologin user_autologin_pkey; Type: CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY user_autologin
    ADD CONSTRAINT user_autologin_pkey PRIMARY KEY (key_id, user_id);


--
-- Name: user_profiles user_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY user_profiles
    ADD CONSTRAINT user_profiles_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey1; Type: CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey1 PRIMARY KEY (id);


--
-- Name: autoidx_addition; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_addition ON rawdata USING gin (addition gin_trgm_ops);


--
-- Name: autoidx_antonym_e; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_antonym_e ON rawdata USING gin (antonym_e gin_trgm_ops);


--
-- Name: autoidx_avr_score; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_avr_score ON rawdata USING gin (avr_score gin_trgm_ops);


--
-- Name: autoidx_category; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_category ON rawdata USING gin (category gin_trgm_ops);


--
-- Name: autoidx_category_2; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_category_2 ON rawdata USING gin (category_2 gin_trgm_ops);


--
-- Name: autoidx_explanation; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_explanation ON rawdata USING gin (explanation gin_trgm_ops);


--
-- Name: autoidx_for_glossary; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_for_glossary ON rawdata USING gin (for_glossary gin_trgm_ops);


--
-- Name: autoidx_for_glossary2; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_for_glossary2 ON rawdata USING gin (for_glossary2 gin_trgm_ops);


--
-- Name: autoidx_phrease_group; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_phrease_group ON rawdata USING gin (phrease_group gin_trgm_ops);


--
-- Name: autoidx_pronunciation; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_pronunciation ON rawdata USING gin (pronunciation gin_trgm_ops);


--
-- Name: autoidx_relatedword; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_relatedword ON rawdata USING gin (relatedword gin_trgm_ops);


--
-- Name: autoidx_synonim_e; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_synonim_e ON rawdata USING gin (synonim_e gin_trgm_ops);


--
-- Name: autoidx_synonim_j; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_synonim_j ON rawdata USING gin (synonim_j gin_trgm_ops);


--
-- Name: autoidx_term_e; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_term_e ON rawdata USING gin (term_e gin_trgm_ops);


--
-- Name: autoidx_text; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_text ON rawdata USING gin (text gin_trgm_ops);


--
-- Name: autoidx_topic; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_topic ON rawdata USING gin (topic gin_trgm_ops);


--
-- Name: autoidx_translation_j; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX autoidx_translation_j ON rawdata USING gin (translation_j gin_trgm_ops);


--
-- Name: idx_comment_comment; Type: INDEX; Schema: public; Owner: genetics
--

CREATE INDEX idx_comment_comment ON comments USING gin (comment gin_trgm_ops);


SET search_path = genet, pg_catalog;

--
-- Name: usernames update_users_modified_column; Type: TRIGGER; Schema: genet; Owner: genetics
--

CREATE TRIGGER update_users_modified_column BEFORE UPDATE ON usernames FOR EACH ROW EXECUTE PROCEDURE public.update_modified_column();


SET search_path = public, pg_catalog;

--
-- Name: login_attempts update_login_attempts_update_column; Type: TRIGGER; Schema: public; Owner: genetics
--

CREATE TRIGGER update_login_attempts_update_column BEFORE UPDATE ON login_attempts FOR EACH ROW EXECUTE PROCEDURE update_time_column();


--
-- Name: user_autologin update_user_autologin_last_login_column; Type: TRIGGER; Schema: public; Owner: genetics
--

CREATE TRIGGER update_user_autologin_last_login_column BEFORE UPDATE ON user_autologin FOR EACH ROW EXECUTE PROCEDURE update_last_login_column();


--
-- Name: users update_users_modified_column; Type: TRIGGER; Schema: public; Owner: genetics
--

CREATE TRIGGER update_users_modified_column BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE update_modified_column();


SET search_path = genet, pg_catalog;

--
-- Name: article article_atype_id_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY article
    ADD CONSTRAINT article_atype_id_fkey FOREIGN KEY (atype_id) REFERENCES article_type(atype_id);


--
-- Name: article article_jterm_id_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY article
    ADD CONSTRAINT article_jterm_id_fkey FOREIGN KEY (jterm_id) REFERENCES j_term(jterm_id);


--
-- Name: dicipline dicipline_dupdateuser_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY dicipline
    ADD CONSTRAINT dicipline_dupdateuser_fkey FOREIGN KEY (dupdateuser) REFERENCES usernames(user_id);


--
-- Name: e_antonym e_antonym_eterm_id_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY e_antonym
    ADD CONSTRAINT e_antonym_eterm_id_fkey FOREIGN KEY (eterm_id) REFERENCES e_term(eterm_id);


--
-- Name: e_term e_term_eupdateuser_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY e_term
    ADD CONSTRAINT e_term_eupdateuser_fkey FOREIGN KEY (eupdateuser) REFERENCES usernames(user_id);


--
-- Name: j_term j_term_dicipline_id_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY j_term
    ADD CONSTRAINT j_term_dicipline_id_fkey FOREIGN KEY (dicipline_id) REFERENCES dicipline(dicipline_id);


--
-- Name: j_term j_term_jupdateuser_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY j_term
    ADD CONSTRAINT j_term_jupdateuser_fkey FOREIGN KEY (jupdateuser) REFERENCES usernames(user_id);


--
-- Name: jterm_eterm_link jterm_eterm_link_eterm_id_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY jterm_eterm_link
    ADD CONSTRAINT jterm_eterm_link_eterm_id_fkey FOREIGN KEY (eterm_id) REFERENCES e_term(eterm_id);


--
-- Name: jterm_eterm_link jterm_eterm_link_jterm_id_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY jterm_eterm_link
    ADD CONSTRAINT jterm_eterm_link_jterm_id_fkey FOREIGN KEY (jterm_id) REFERENCES j_term(jterm_id);


--
-- Name: phrase_group phrase_group_pgupdateuser_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY phrase_group
    ADD CONSTRAINT phrase_group_pgupdateuser_fkey FOREIGN KEY (pgupdateuser) REFERENCES usernames(user_id);


--
-- Name: priority priority_jterm_id_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY priority
    ADD CONSTRAINT priority_jterm_id_fkey FOREIGN KEY (jterm_id) REFERENCES j_term(jterm_id);


--
-- Name: priority_levels priority_levels_plupdateuser_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY priority_levels
    ADD CONSTRAINT priority_levels_plupdateuser_fkey FOREIGN KEY (plupdateuser) REFERENCES usernames(user_id);


--
-- Name: priority priority_plevel_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY priority
    ADD CONSTRAINT priority_plevel_fkey FOREIGN KEY (plevel) REFERENCES priority_levels(plevel);


--
-- Name: priority priority_pupdateuser_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY priority
    ADD CONSTRAINT priority_pupdateuser_fkey FOREIGN KEY (pupdateuser) REFERENCES usernames(user_id);


--
-- Name: rating rating_jterm_id_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY rating
    ADD CONSTRAINT rating_jterm_id_fkey FOREIGN KEY (jterm_id) REFERENCES j_term(jterm_id);


--
-- Name: related_terms related_terms_jterm_id_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY related_terms
    ADD CONSTRAINT related_terms_jterm_id_fkey FOREIGN KEY (jterm_id) REFERENCES j_term(jterm_id);


--
-- Name: related_terms related_terms_relupdateuser_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY related_terms
    ADD CONSTRAINT related_terms_relupdateuser_fkey FOREIGN KEY (relupdateuser) REFERENCES usernames(user_id);


--
-- Name: usernames users_uupdateuser_fkey; Type: FK CONSTRAINT; Schema: genet; Owner: genetics
--

ALTER TABLE ONLY usernames
    ADD CONSTRAINT users_uupdateuser_fkey FOREIGN KEY (uupdateuser) REFERENCES usernames(user_id);


SET search_path = public, pg_catalog;

--
-- Name: comments comment_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comment_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: rawdata_archive rawdata_archive_user_id_deleted_fkey; Type: FK CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY rawdata_archive
    ADD CONSTRAINT rawdata_archive_user_id_deleted_fkey FOREIGN KEY (user_id_deleted) REFERENCES users(id);


--
-- Name: rawdata_archive rawdata_archive_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY rawdata_archive
    ADD CONSTRAINT rawdata_archive_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: rawdata rawdata_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: genetics
--

ALTER TABLE ONLY rawdata
    ADD CONSTRAINT rawdata_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

